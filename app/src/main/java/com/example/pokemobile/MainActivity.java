package com.example.pokemobile;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.goodiebag.pinview.Pinview;

public class MainActivity extends AppCompatActivity{

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final String password = "1234";
        Pinview pinview = findViewById(R.id.pinView);
        pinview.setPinViewEventListener(new Pinview.PinViewEventListener(){
            public void onDataEntered(Pinview pinview, boolean b){

                String userInput = pinview.getValue();
                if (userInput.equals(password)){
                    Toast.makeText(MainActivity.this, pinview.getValue() + ", correct.",
                        Toast.LENGTH_SHORT).show();
                        loadReg();

                }
                else{
                    Toast.makeText(MainActivity.this, pinview.getValue() + ", incorrect.",
                            Toast.LENGTH_SHORT).show();
                    pinview.setPinViewEventListener(null);
                    loadSelf();
                }
            }
        });
    }

    void loadSelf()
    {
        startActivity(new Intent(this, MainActivity.class));
    }

    void loadReg()
    {
        startActivity(new Intent(this, RegisterActivity.class));
    }
}
