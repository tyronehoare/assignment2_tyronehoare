package com.example.pokemobile;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener{

    private Button btnRegister;
    private EditText textEmail;
    private EditText textPassword;
    private TextView textSignIn;
    private ProgressDialog progressDialog;
    private FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        firebaseAuth = FirebaseAuth.getInstance();
        progressDialog = new ProgressDialog(this);

        btnRegister = findViewById(R.id.btnRegister);
        textEmail = findViewById(R.id.textEmail);
        textPassword = findViewById(R.id.textPassword);
        textSignIn = findViewById(R.id.textRegister);

        btnRegister.setOnClickListener(this);
        textSignIn.setOnClickListener(this);


    }

    private void registerUser(){
        String email = textEmail.getText().toString().trim();
        String password = textPassword.getText().toString().trim();

        if(TextUtils.isEmpty(email)){
            //Email is empty.
            Toast.makeText(this,
                    "Please enter Email Address.", Toast.LENGTH_SHORT).show();
            return;
        }

        if(TextUtils.isEmpty(password)){
            //Password is empty.
            Toast.makeText(this,
                    "Please enter Password.", Toast.LENGTH_SHORT).show();
            return;
        }

        progressDialog.setMessage("Registering user with data.");
        progressDialog.show();
        firebaseAuth.createUserWithEmailAndPassword(email, password).
                addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()){
                            //On Success
                            Toast.makeText(RegisterActivity.this, "Registered Successfully",
                                    Toast.LENGTH_SHORT).show();
                            progressDialog.hide();

                            finish();

                            if(firebaseAuth.getCurrentUser() != null){
                                //profile activity
                                finish();
                                startActivity(new Intent(getApplicationContext(), ProfileActivity.class));
                            }
                        }
                        else{
                            //On Fail
                            Toast.makeText(RegisterActivity.this, "Register Failed.\nTry Again.",
                                    Toast.LENGTH_SHORT).show();
                            progressDialog.hide();
                        }
                    }
                });
    }

    @Override
    public void onClick(View v) {
        if(v == btnRegister){
            registerUser();
        }
        else if(v == textSignIn){
            //Open login activity
            startActivity(new Intent(this, LoginActivity.class));
        }

    }
}

